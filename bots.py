import math
import random
import json


class World:
    # a World just holds a bunch of bots
    def __init__(self, width, height, numBots):
        self.width = width
        self.height = height
        self.bots = [Bot(self) for i in range(numBots)]
        self.foods = []

    def AddFood(self, x=None, y=None):
        newFood = Food(0, 0)
        x = x or random.randint(0, self.width - newFood.width)
        y = y or random.randint(0, self.height - newFood.height)
        newFood.x = x
        newFood.y = y
        self.foods.append(newFood)
        self._KeepInBounds(newFood)

    def ClearFood(self):
        self.foods = []

    def _KeepInBounds(self, obj):
        # keep bots within the screen limits
        if obj.x <= 0:
            obj.x = 1

        elif obj.x > self.width - obj.width:
            obj.x = self.width - obj.width

        if obj.y <= 0:
            obj.y = 1

        elif obj.y > self.height - obj.height:
            obj.y = self.height - obj.height


class Food:
    def __init__(self, x, y):
        self.width = 10
        self.height = 10

        self.x = x
        self.y = y

    @property
    def coords(self):
        return self.x, self.y, self.x + self.width, self.y + self.height

    def Overlaps(self, x1, y1, x2, y2):
        if x1 <= self.x <= x2 and \
                y1 <= self.y <= y2:
            return True

        return False


class Bot:
    def __init__(self, world, x=None, y=None):
        self.width = 25
        self.height = 25
        self.world = world

        self.x = x or random.randint(0, self.world.width - self.width)
        self.y = y or random.randint(0, self.world.height - self.height)
        self.brain = _Brain(
            host=self,
            numInputs=4,  # up, down, left, right, diagonals x4
            numOutputs=4  # updown left right
        )

        self.foodEaten = 0
        self.HEALTH_BOOST = 100
        self.health = self.HEALTH_BOOST
        self.closestFood = None

    def ResetHealth(self):
        self.foodEaten = 0
        self.health = self.HEALTH_BOOST

    def SetBrain(self, brain=None):
        if brain:
            self.brain = brain
        else:
            # generate random brain
            self.brain = _Brain(
                self,
                numInputs=self.brain.numInputs,
                numOutputs=self.brain.numOutputs,
            )

    @property
    def coords(self):
        return self.x, self.y, self.x + self.width, self.y + self.height

    def SenseOverlapping(self):
        ret = 0
        for bot in self.world.bots:
            if bot != self:
                if self.Overlaps(*bot.coords):
                    ret += 1
        return ret  # number of bots overlapping this one

    def ClosestFood(self):
        if self.closestFood:
            return self.closestFood

        closestFood = None
        smallestDelta = None
        for food in self.world.foods:
            thisDelta = self.DeltaFrom(food.x, food.y)
            # print('thisDelta=', thisDelta)
            if closestFood is None or thisDelta < smallestDelta:
                closestFood = food
                smallestDelta = thisDelta

        self.closestFood = closestFood
        return closestFood

    def SenseUp(self):
        # return 1 if there is food above
        if self.ClosestFood() and self.ClosestFood().x < self.x:
            return 1
        return 0

    def SenseDown(self):
        # return 1 if there is food above=
        if self.ClosestFood() and self.ClosestFood().x > self.x:
            return 1
        return 0

    def SenseLeft(self):
        # return 1 if there is food above
        if self.ClosestFood() and self.ClosestFood().x < self.y:
            return 1
        return 0

    def SenseRight(self):
        # return 1 if there is food above
        if self.ClosestFood() and self.ClosestFood().x > self.y:
            return 1
        return 0

    def Overlaps(self, x1, y1, x2, y2):
        if x1 < self.x < x2 and \
                y1 < self.y < y2:
            return True

        return False

    def DeltaFrom(self, otherX, otherY):
        deltaX = otherX - self.x
        deltaY = otherY - self.y
        delta = math.sqrt(math.pow(deltaX, 2) + math.pow(deltaY, 2))
        return delta

    def Move(self):
        # use self.brain to decide how to move and update self.x and self.y
        # self.x += random.randint(-1, 1)
        # self.y += random.randint(-1, 1)

        self.health -= 1

        if self.health < 0:
            return

        muscles = self.brain.Think(
            self.SenseUp(),
            self.SenseDown(),
            self.SenseLeft(),
            self.SenseRight()
        )
        self.x += muscles[0]
        self.x -= muscles[1]

        self.y += muscles[2]
        self.y -= muscles[3]

        self.world._KeepInBounds(self)

        # "eat" any food nearby
        for food in self.world.foods:
            if food.Overlaps(*self.coords):
                self.foodEaten += 1
                self.world.foods.remove(food)
                self.health += self.HEALTH_BOOST # when u eat your health goes up

        # reset closest food to None
        self.closestFood = None

    def __str__(self):
        return f'<Bot: id={id(self)}, x={self.x}, y={self.y}>'


class _Brain:
    def __init__(self, host, numInputs, numOutputs):
        self._host = host
        self.numInputs = numInputs
        self.numOutputs = numOutputs

        self._sensors = [_Sensor() for i in range(numInputs)]
        self._neurons = [_Neuron() for i in range(random.randint(0, 100))]
        self._muscles = [_Muscle() for i in range(numOutputs)]

        self.MakeConnections()

    def MakeConnections(self, connectionMap=None):
        if connectionMap is None:  # make random connections
            for i in range(random.randint(1, self._host.world.width)):
                n1 = None
                n2 = None

                n1 = random.choice(self._sensors + self._neurons + self._muscles)
                while True:
                    n2 = random.choice(self._sensors + self._neurons + self._muscles)

                    if n1 != n2:
                        break

                n1.Connect(n2)

    def Mutate(self, mutationPercet):
        print('Mutate(percent=', mutationPercet)
        # randomly make mutations
        totalConnections = 0
        allNeurons = list(self._sensors + self._neurons + self._muscles).copy()
        #print('allNeurons=', allNeurons)

        numberOfMutations = int(len(allNeurons) * (mutationPercet / 100))
        if numberOfMutations is 0:  # to keep from being 0
            numberOfMutations = 1
        #print('numberOfMutations=', numberOfMutations)

        neuronsToMutate = []
        for i in range(numberOfMutations):
            n = random.choice(allNeurons)
            allNeurons.remove(n)
            neuronsToMutate.append(n)

        #print('neuronsToMutate=', neuronsToMutate)

        DELETE_CONNECTION = 'Delete Connection'
        ADD_CONNECTION = 'Add Connection'
        CHANGE_WEIGHT = 'Change Weight'
        NEW_NEURON = 'New Neuron'

        typeOfMutations = [DELETE_CONNECTION, ADD_CONNECTION, CHANGE_WEIGHT, NEW_NEURON]
        for neuron in neuronsToMutate:
            mutationType = random.choice(typeOfMutations)
            #print('mutationType=', mutationType)
            if mutationType == DELETE_CONNECTION:
                if neuron.connections:
                    #print('neuron.connections=', neuron.connections)
                    neuron.connections.pop(random.choice(list(neuron.connections.keys())))

            elif mutationType == ADD_CONNECTION:
                otherNeuron = random.choice(
                    self._sensors + self._neurons + self._muscles
                )
                neuron.Connect(otherNeuron)

            elif mutationType == CHANGE_WEIGHT:
                if neuron.connections:
                    #print('neuron.connections=', neuron.connections)
                    key = random.choice(list(neuron.connections.keys()))
                    oldValue = neuron.connections.pop(key)
                    neuron.Connect(key)  # re-make the same connection will choose a random weight

            elif mutationType == NEW_NEURON:
                self._neurons.append(_Neuron())

        # remove any neurons with no connections to anything else
        neuronsWithNoConnections = []
        for neuron in self._neurons:
            hasConnection = False
            for otherNeuron in self._neurons + self._sensors + self._muscles:
                if neuron != otherNeuron:
                    if neuron in otherNeuron.connections:
                        hasConnection = True
                        break
            if not hasConnection:
                neuronsWithNoConnections.append(neuron)

        for n in neuronsWithNoConnections:
            #print('removing n with no connections')
            self._neurons.remove(n)

    def Think(self, *inputs):
        # print('Think(inputs=', inputs)
        for index, sensor in enumerate(self._sensors):
            # print('line 62', index, sensor)
            sensor.Process(inputs[index])

        for neuron in self._neurons:
            neuron.Process()

        for muscle in self._muscles:
            muscle.Process()

        return tuple([m.output for m in self._muscles])

    def __str__(self):
        return f'<{type(self).__name__}: sensors={self._sensors}, neurons={self._neurons}, muscles={self._muscles}'


class _Neuron:
    def __init__(self):
        self.connections = {
            # otherNeuronObject: weight
        }
        self.output = random.randint(0, 1)

    def Connect(self, otherNeuron):
        # print(f'Connecting self={self} > other={otherNeuron}')
        self.connections[otherNeuron] = random.randint(-100, 100)

    def Process(self):
        weightedSum = 0
        for otherNeuron, weight in self.connections.items():
            weightedSum += otherNeuron.output * weight

        ret = 1 if weightedSum > 0 else 0
        self.output = ret
        return ret

    def __str__(self):
        return f'<{type(self).__name__}: id={id(self)}, connections={len(self.connections)}>'

    def __repr__(self):
        return str(self)


class _Sensor(_Neuron):
    # just a neuron that is connected to the outside world
    def Process(self, output=None):
        self.output = output
        return output


class _Muscle(_Neuron):
    # just a neuron at the output
    pass
