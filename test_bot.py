from canvas import GrantCanvas
from bots import World

SIZE = 200
world = World(SIZE, SIZE, numBots=1)  # int((SIZE * BOT_DENSITY) / 50))

grantCanvas = GrantCanvas(world)

world.AddFood(
    world.width / 2,
    world.height / 2
)
grantCanvas.Draw()

bot = world.bots[0]

print('bot.ClosestFood()=', bot.ClosestFood())


grantCanvas.mainloop()