import json

from canvas import GrantCanvas
from bots import World
from threading import Timer
import time
import random
import math
import copy

SIZE = 800
BOT_DENSITY = 1
FOOD_DENSITY = 20

world = World(SIZE, SIZE, numBots=int((SIZE * BOT_DENSITY) / 50))

grantCanvas = GrantCanvas(world)


# def GetFitness(bot):
#     # bots should go to the bottom right
#     return (bot.x + bot.y) * 2


def GetFitness(bot):
    closestFood = bot.ClosestFood()
    if closestFood:
        delta = bot.DeltaFrom(closestFood.x, closestFood.y)
        bonus = (world.height / delta) * 2
    else:
        bonus = 0

    return bot.foodEaten


def SomeoneIsAlive():
    for bot in world.bots:
        if bot.health > 0:
            # print('bot.health=', bot.health)
            return True
    return False


def SetRandomLocations():
    # reset bots
    for bot in world.bots:
        # bot.x = random.randint(0, world.width)
        # bot.y = random.randint(0, world.height)

        # bot.x = 0
        # bot.y = 0

        bot.x = world.width / 2
        bot.y = world.height / 2

        bot.ResetHealth()

    # reset food
    world.ClearFood()
    for i in range(int(FOOD_DENSITY * len(world.bots))):
        # world center
        # x = world.width / 2 + random.randint(-world.width / 4, world.width / 4)
        # y = world.height / 2 + random.randint(-world.height / 4, world.height / 4)

        # wold sides
        # x = random.randint(0, world.width / 4) if random.randint(0, 1) else random.randint(3 * world.width / 4,
        #                                                                                    world.width)
        # y = random.randint(0, world.height / 4) if random.randint(0, 1) else random.randint(3 * world.height / 4,
        #                                                                                     world.height)
        # world.AddFood(x, y)

        # world random
        world.AddFood(
            random.randint(0, world.width),
            random.randint(0, world.height)
        )


def SaveBrain(brain):
    d = dict(brain)
    with open('brain.json', mode='wt') as file:
        file.write(json.dumps(brain, indent=2, sort_keys=True))


SetRandomLocations()


def Loop():
    generation = 0
    while True:
        # time.sleep(0.01)
        print('loop')

        # for i in range(int(world.height * 1.5)):
        i = 0
        while SomeoneIsAlive():
            i += 1
            for bot in world.bots:
                bot.Move()

                if i % 10 == 0:
                    grantCanvas.Draw()

        bots = sorted(world.bots, key=lambda b: GetFitness(b))  # index 0 is worst
        bestScore = GetFitness(bots[-1])
        print('bestScore=', bestScore)
        grantCanvas.BestScore(bestScore)
        print('worstScore=', GetFitness(bots[0]))

        bestBrain = bots[-1].brain
        print('bestBrain=', bestBrain)

        # keep the best 25% of brains

        # # 25% get a best-but-mutated brain
        # for bot in bots[:int(len(bots) / 4)]:
        #     # if GetFitness(bot) < midpoint:
        #     bot.SetBrain(copy.copy(bestBrain))
        #     bestBrain.Mutate(15)
        #
        # # 25% get a new brain
        # for bot in bots[int(len(bots) / 4): int(len(bots) / 2)]:
        #     bot.SetBrain() # new brain

        # clone best brain x3
        for index, bot in enumerate(bots[1:-1]):
            bot.brain.Mutate(100 - (5 * index))

        bots[0].SetBrain()  # new brain

        generation += 1
        print('Generation', generation)
        grantCanvas.Generation(generation)
        time.sleep(1)
        SetRandomLocations()


Timer(0, Loop).start()

grantCanvas.mainloop()
