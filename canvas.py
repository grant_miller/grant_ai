import random
from tkinter import Tk, Canvas, Label,Checkbutton, IntVar
from threading import Timer
from bots import Food

COLORS = ['red', 'orange', 'yellow', 'green', 'blue', 'cyan', 'purple', 'white', 'black',
          'grey', 'magenta']


def GetColor():
    while True:
        for item in COLORS:
            yield item


class GrantCanvas:
    def __init__(self, world):
        self._world = world
        self._objs = {
            # obj: canvasID,
        }
        self._root = Tk()
        self._root.title('Grant AI')

        self._canvas = Canvas(self._root, width=world.width, height=world.height)
        self._canvas.pack()

        self._lblBestScore = Label(self._root, text='Best Score')
        self._lblBestScore.pack()

        self._lblGeneration = Label(self._root, text='Generaton: 0')
        self._lblGeneration.pack()

        self._varChkDraw = IntVar()
        self._varChkDraw.set(1)
        self._chkDraw = Checkbutton(self._root, text='Draw', var=self._varChkDraw)
        self._chkDraw.pack()

        self._topScore = 0
        self._lblTopScore = Label(self._root, text='Top Score: 0')
        self._lblTopScore.pack()

    def Draw(self):
        if self._varChkDraw.get():
            for bot, color in zip(self._world.bots, GetColor()):
                canvasID = self._objs.get(bot, None)
                if canvasID is None:
                    canvasID = self._canvas.create_rectangle(*bot.coords, fill=color)
                    self._objs[bot] = canvasID

                self._canvas.coords(canvasID, bot.coords)

            # draw food
            for food in self._world.foods:
                canvasID = self._objs.get(food, None)
                if canvasID is None:
                    canvasID = self._canvas.create_oval(*food.coords, fill='red')
                    self._objs[food] = canvasID
                # food doesnt move

            # remove any food that has been eaten
            for obj in self._objs.copy():
                if type(obj) == Food:
                    food = obj
                    if food not in self._world.foods:
                        canvasID = self._objs.get(food)
                        self._canvas.delete(canvasID)
                        self._objs.pop(food)

    def mainloop(self):
        self._root.mainloop()

    def BestScore(self, score):
        self._lblBestScore.config(text=f'Best Score: {int(score)}')
        if int(score > self._topScore):
            self._lblTopScore.config(text=f'Top Score: {int(score)}')
            self._topScore = score

    def Generation(self, gen):
        self._lblGeneration.config(text=f'Generation: {gen}')
